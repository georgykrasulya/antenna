import os, sys, site
site.addsitedir(os.path.join(os.path.expanduser('~'), 'site-packages'))

sys.path.insert(0, os.path.join(os.path.expanduser('~'), 'flask-projects/antenna'))
sys.path.insert(0, os.path.expanduser('~'))

import wtforms

import antenna
application = antenna.app