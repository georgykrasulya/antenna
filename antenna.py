import settings
from flask import Flask, g, render_template
from flaskext.uploads import UploadSet, IMAGES, configure_uploads
from flaskext.sqlalchemy import SQLAlchemy

try:
	from flaskext.debugtoolbar import DebugToolbarExtension
except ImportError:
	# no such module
	pass

app = Flask(__name__)
app.config.from_object(settings)

db = SQLAlchemy(app)
try:
	toolbar = DebugToolbarExtension(app)
except NameError:
	# no such module
	pass

posts_set = UploadSet('posts', IMAGES)
products_set = UploadSet('products', IMAGES)
providers_set = UploadSet('providers', IMAGES)

configure_uploads(app, (posts_set, products_set, providers_set))

from views.frontend import frontend
from views.admin import admin

app.register_module(frontend)
app.register_module(admin, url_prefix='/admin')