# coding: utf-8

from flask import Module, session, flash, redirect, render_template, request, g
from functools import wraps
from flaskext import sqlalchemy

from antenna import app, db
from forms import *
from models import *

admin = Module(__name__)

def login_required(f):
	@wraps(f)
	def decorated(*args, **kwargs):
		if not logged():
			flash(u'Вы должны авторизоваться!', 'error')
			return redirect('/admin/login')
		return f(*args, **kwargs)
	return decorated

def logged():
	return session.get('logged', False)

@admin.route('/')
@login_required
def index():
	g.categories = Category.query.all()

	return render_template('admin/index.html')

@admin.route('/category/<int:id>')
@login_required
def category(id):
	g.category = Category.query.get(id)

	return render_template('admin/category.html')

def add_record(template_name='/admin/manage_record.html'):
	def _add_record(cls_name):
		FormClass = globals()['%sForm' % cls_name]
		ModelClass = globals()[cls_name]

		g.form = FormClass(request.form)
		g.add = True
		g.cls_name = cls_name
		g.record = 123

		if request.method == 'POST' and g.form.validate():
			record = Category(**request.form)

			db.session.add(record)
			db.session.commit()
			flash(u'Запись добавлена')

			if '_add_more' in request.form:
				redirect_url = '/admin/%s/new' % cls_name
			else:
				redirect_url = '/admin'
			return redirect(redirect_url)

		return render_template(template_name)
	return _add_record
admin.add_url_rule('/<cls_name>/new', 'add_record', login_required(add_record()), methods=['GET', 'POST'])

@admin.route('/<cls_name>/<int:id>', methods=['GET', 'POST'])
@login_required
def edit_record(cls_name, id):
	FormClass = globals()['%sForm' % cls_name]
	ModelClass = globals()[cls_name]

	g.record = record = ModelClass.query.get(id)

	g.form = FormClass(request.form, obj=record)
	g.cls_name = cls_name

	if request.method == 'POST' and g.form.validate():
		record.setattrs(**request.form)

		if 'photo' in request.files and request.files['photo']:
			product.save_photo(request.files['photo'])

		db.session.add(record)
		db.session.commit()
		flash(u'Запись изменена')

		if '_add_more' in request.form:
			redirect_url = '/admin/%s/new' % cls_name
		else:
			redirect_url = '/admin'
		return redirect(redirect_url)

	return render_template('/admin/manage_record.html')

@admin.route('/category/new', methods=['GET', 'POST'])
@login_required
def add_category():
	form = CategoryForm(request.form, category_id=request.args.get('category_id', None))

	if request.method == 'POST' and form.validate():
		category = Category(
			title=request.form['title'],
			slug=request.form['slug'],
		)
		db.session.add(category)
		db.session.commit()
		flash(u'Категория "%s" добавлена' % category.title)
		redirect_url = '/admin/category/new' if '_add_more' in request.form \
			else '/admin'
		return redirect(redirect_url)

	context = dict(
		form=form,
		title=u'Добавление категории',
		add=True,
	)

	g.model = 'category'

	return render_template('admin/manage_record.html', **context)

@admin.route('/product/new', methods=['GET', 'POST'])
@login_required
def add_product():
	form = ProductForm(request.form, category_id=request.args.get('category_id', None))
	categories = Category.query.all()
	form.category_id.choices = [(c.id, c.title) for c in categories]

	if request.method == 'POST' and form.validate():
		product = Product(
			title=request.form['title'],
			description=request.form['description'],
			photo=request.files['photo'],
			category_id=request.form['category_id'],
		)
		db.session.add(product)
		db.session.commit()
		flash(u'Модель "%s" добавлена' % product.title)
		redirect_url = '/admin/product/new' if '_add_more' in request.form \
			else '/admin/category/%s' % request.form['category_id']
		return redirect(redirect_url)

	context = dict(
		form=form,
		title=u'Добавление модели',
		add=True,
	)

	g.model = 'product'

	return render_template('admin/manage_record.html', **context)

@admin.route('/product/<int:id>', methods=['GET', 'POST'])
@login_required
def edit_product(id):
	product = Product.query.get(id)
	categories = Category.query.all()
	form = ProductForm(request.form, obj=product)
	form.category_id.choices = [(c.id, c.title) for c in categories]

	if request.method == 'POST' and form.validate():

		product.title = request.form['title']
		product.description = request.form['description']
		product.category_id = request.form['category_id']

		if 'photo' in request.files and request.files['photo']:
			product.save_photo(request.files['photo'])

		db.session.add(product)
		db.session.commit()
		flash(u'Модель "%s" изменена' % product.title)
		redirect_url = '/admin/category/%s' % product.category_id
		return redirect(redirect_url)

	context = dict(
		form=form,
		title=u'Редактирование модели "%s"' % product.title,
		product=product,
	)

	g.model = 'product'
	g.id = product.id

	return render_template('admin/manage_record.html', **context)

@admin.route('/product/delete/<int:id>')
@login_required
def delete_product(id):
	is_ajax = request.args.get('ajax', False)

	if not id is None:
		try:
			product = Product.query.get(id)
		except sqlalchemy.orm.exc.UnmappedInstanceError:
			product = None
			
		if product:
			db.session.delete(product)
			db.session.commit()

			if is_ajax:
				return 'ok'
			else:
				flash(u'Модель была удалена')
				return redirect('/admin/category/%s' % product.category_id)
	if is_ajax:
		return 'error'
	else:
		flash(u'Произошла ошибка', 'error')
		return redirect('/admin')

@admin.route('/login', methods=['GET', 'POST'])
def login():
	form = LoginForm(request.form)

	if request.method == 'POST' and form.validate():
		if (request.form['username'] == app.config['ADMIN_USERNAME'] and
		request.form['password'] == app.config['ADMIN_PASSWORD']):
			flash(u'Вы усешно авторизовались.')
			session['logged'] = True
			return redirect('/admin')
		flash(u'Неправильные логин и/или пароль.', 'error')

	body_class = 'class="login"'

	context = dict(
		form=form,
		body_class=body_class,
		not_logged=True,
	)

	return render_template('admin/login.html', **context)

@admin.route('/admin/logout')
def logout():
	session['logged'] = False
	return render_template('admin/logout.html', not_logged=True)