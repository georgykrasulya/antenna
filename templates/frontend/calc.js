(function() {

	var dependenciesObject = {
		card: [],
		catalog: [],
		shop: []
	},
	pageDesignPrice = {{ page_design_price }}

	{% for price in prices %}
		{% if price.by_default %}
			dependenciesObject.card.push({{ price.id }});
			dependenciesObject.catalog.push({{ price.id }});
			dependenciesObject.shop.push({{ price.id }});
		{% else %}
			{% if price.in_card %}dependenciesObject.card.push({{ price.id }});{% endif %}
			{% if price.in_catalog %}dependenciesObject.catalog.push({{ price.id }});{% endif %}
			{% if price.in_shop %}dependenciesObject.shop.push({{ price.id }});{% endif %}
		{% endif %}
	{% endfor %}

	var calc = $('calc');

	if (calc) {

		Locale.use('ru-RU');

		var calcValidator = new Form.Validator.Inline(calc, {
			useTitles: true,
			serial: false,
			errorPrefix: ''
		});
		
		var packs = $('packs'),
			packsArr = ['card', 'catalog', 'shop', 'other'],
			packLinks = packs.getChildren('a'),
			currentPrice = 0,
			currentPages = 0,
			currentPackIndex,
			calcCheckboxes = calc.getElements('input'),
			totalPrice = $('totalPrice').getElement('span'),
			designPrice = $('designPrice'),
			priceDescriptions = calc.getElements('a.description'),
			makeOrder = $('makeOrder'),
			orderFieldset = $('orderFieldset'),
			cancelOrder = $('cancelOrder'),
			print = $('print'),
			windowScroll = new Fx.Scroll(window);

		function showOrderFieldset() {
			orderFieldset.setStyle('display', 'block');
			// makeOrder.set({
			// 	'html': 'закрыть форму',
			// 	'title': 'Не нужно отправлять форму'
			// });
			$('name').focus();

			windowScroll.start(0, orderFieldset.getPosition().y - 100);
		}

		function hideOrderFieldset() {
			orderFieldset.setStyle('display', 'none');
			// makeOrder.set({
			// 	'html': 'заполнить форму',
			// 	'title': 'Форма откроется прямо здесь'
			// });

			windowScroll.start(0, calc.getPosition().y - 100);
		}

		print.addEvent('click', function() {
			window.print();
		
			return false;	
		});

		calc.set('send', {
			url: ('/send-feedback'),
			method: 'POST',

			onComplete: function(res) {
				hideOrderFieldset();
				calc.reset();
			}
		})
		calc.addEvent('submit', function() {
			if (calcValidator.validate())
				calc.send();
			
			return false;
		});

		makeOrder.addEvent('click', function() {
			var shouldShow = orderFieldset.getStyle('display') == 'none';
			shouldShow ? showOrderFieldset() : hideOrderFieldset();

			return false;
		});

		cancelOrder.addEvent('click', function() {
			hideOrderFieldset();

			return false;
		});

		$('totalPrice').setStyle('display', 'block');

		packLinks.each(function(pack, i) {

			var dependencies = dependenciesObject[packsArr[i]];

			if (pack.hasClass('selected')) currentPackIndex = i;

			pack.addEvent('click', function() {
				selectPack(i);

				if (i != 3) {
					calcCheckboxes.each(function(el) {
						el.checked = false;
						el.fireEvent('change');
					});

					dependencies.each(function(index) {
						var price = $('price' + index);
						price.checked = true;
						price.fireEvent('change');
					});

					countPrice();
				}

				History.pushState({ state: i }, null, '/calculator/' + packsArr[i]);

				return false;
			});
		});

		calcCheckboxes.each(function(input) {
			new CustomFormElement(input);

			input.addEvent('click', function() {
				if (currentPackIndex != 3) selectPack(3);
			});

			input.addEvent('change', function() {
				var row = this.getParent('.form-row');

				if (row)
					row[(this.checked ? 'add' : 'remove') + 'Class']('selected');

				countPrice();
			});
		});

		new Tips(priceDescriptions, {
			className: 'tip-handler',
			fixed: true,
			offset: { x: 35, y: -16 }
		});
		priceDescriptions.addEvent('click', function() {
			return false;	
		});

		function selectPack(i) {
			packLinks.removeClass('selected');
			packLinks[i].addClass('selected');
			currentPackIndex = i;
		}

		function countPrice() {
			currentPrice = 0;
			currentPages = 0;
			calcCheckboxes.each(function(el, i) {
				if (el.checked) {
					var price = el.get('price');
					if (price)
						currentPrice += parseInt(el.get('price'), 10);
					// currentPrice += parseInt(el.get('min_pages')) * pageDesignPrice;

					currentPages += parseInt(el.get('min_pages'));
				}
			});

			// designPrice.set('html', 'от ' + (4000 + currentPages * pageDesignPrice) + ' руб.');
			totalPrice.set('html', currentPrice.toInt().format(' '));
		}
		countPrice();

	}
	
})();