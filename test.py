from paste.httpserver import serve
from pyramid.config import Configurator
from pyramid.response import Response

def hello_world(req):
	return Response('Hello, world!')

def bye_world(req):
	return Response('Bye-bye!')

if __name__ == '__main__':
	config = Configurator()
	config.add_view(hello_world)
	config.add_view(bye_world, name='bye')

	help(config.add_view)

	app = config.make_wsgi_app()
	serve(app, host='0.0.0.0')