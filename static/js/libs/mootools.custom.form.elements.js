var CustomFormElement = new Class({

	Implements: Options,

	options: {
		cool: true
	},

	initialize: function(el, options) {
		this.setOptions(options);
		this.el = $(el);

		this.el.store('cfe', this);

		this.customize();
	},

	customize: function() {
		if (this.el.type=='checkbox') this.customizeCheckbox();
		if (this.el.type=='radio') this.customizeRadio();
	},

	customizeCheckbox: function() {
		var wrap = new Element('span.wrap', {
			html: '<a href="#" class="checkbox"></a>'
		});
		wrap.wraps(this.el);

		this.wrap = wrap;
		this.customEl = wrap.getFirst();
		this.el.setStyle('display', 'none');

		if (this.el.checked) this.customEl.addClass('checked');
		
		if (! this.el.disabled) {
			this.el.addEvent('change', (function() {
				this.el.checked ?
					this.customEl.addClass('checked') : this.customEl.removeClass('checked');
			}).bind(this));

			this.customEl.addEvent('click', (function() {
				if (this.el.checked) {
					this.el.checked = false;
					this.customEl.removeClass('checked');
				} else {
					this.el.checked = true;
					this.customEl.addClass('checked');
				}
				this.el.fireEvent('change');

				return false;
			}).bind(this));
		} else {
			this.customEl.addEvent('click', function() {
				return false;
			});
		}
	},

	customizeRadio: function() {
		var wrap = new Element('span.wrap.c', {
			html: '<a href="#" class="radio"></a>'
		}),
			otherRadios = this.el.getParents('form')[0].getElements('input[type=radio][name=' + this.el.name + ']');
		wrap.wraps(this.el);

		this.wrap = wrap;
		this.customEl = wrap.getFirst();
		this.el.setStyle('display', 'none');

		if (this.el.checked) this.customEl.addClass('checked');
		
		if (! this.el.disabled) {
			this.el.addEvent('change', (function() {
				otherRadios.each(function(radio, i) {
					radio.retrieve('cfe').customEl.removeClass('checked');
				});
				this.customEl.addClass('checked');
			}).bind(this));

			this.customEl.addEvent('click', (function() {
				if (this.el.checked) {
					this.el.checked = false;
					this.customEl.removeClass('checked');
				} else {
					this.el.checked = true;
					this.customEl.addClass('checked');
				}

				return false;
			}).bind(this));
		} else {
			this.customEl.addEvent('click', function() {
				return false;
			});
		}
	}
});