#!python

import os

DEBUG = 'DEV' in os.environ

# DEBUG = True

if DEBUG:
	PROJECT_DIR = 'g:/work/projects/python/antenna/'
else:
	PROJECT_DIR = os.path.join(os.path.expanduser('~'), 'flask-projects/antenna')

def rel(*x):
	return os.path.join(PROJECT_DIR, *x)

DB_PATH = rel('antenna_dev.db') if DEBUG else rel('antenna.db')

SQLALCHEMY_DATABASE_URI = 'sqlite:///%s' % DB_PATH

SECRET_KEY = '1vlb2n1f2jwhhnblf2y29gn89vl9vb21lwbzuv2c9n1n48v1bn'

ADMIN_USERNAME = 'admin'
ADMIN_PASSWORD = 'kubanm'

DEBUG_TB_INTERCEPT_REDIRECTS = False

# Uploads

UPLOADED_PRODUCTS_DEST = rel('static/products')
UPLOADED_PRODUCTS_URL = '/static/products/'

UPLOADED_PHOTOS_DEST = rel('static/photos')
UPLOADED_PHOTOS_URL = '/static/photos/'

UPLOADED_POSTS_DEST = rel('static/posts')
UPLOADED_POSTS_URL = '/static/posts/'

UPLOADED_PROVIDERS_DEST = rel('static/providers')
UPLOADED_PROVIDERS_URL = '/static/providers/'

DEBUG = True


# def visible(element):
# 	return not (element.parent.name in ['style', 'script', '[document]', 'head', 'title', 'dates']
# 		or re.match('<!--.*-->', str(element),re.UNICODE)):
