# coding: utf-8

from datetime import datetime

from flask import Module, session, flash, redirect, render_template, request, g, abort, jsonify
# from flaskext import sqlalchemy

from _8hands import app, db
from forms import OrderForm
from models import *

frontend = Module(__name__)
try:
	from flaskext.mail import Mail, Message
	mail_app = Mail(app)
except:
	mail_app = False

WORKS_PER_PAGE = 15

# from utils import *

PACKS = (
	'CARD',
	'CATALOG',
	'SHOP',
	'OTHER',
)

ADMIN_IPS = (
	'77.39.91.124',
	'127.0.0.1',
)

PAGE_DESIGN_PRICE = 1000

@app.errorhandler(404)
def error_404(e):
	return '404'

@frontend.context_processor
def context_pass_year():
	show_admin_link = request.remote_addr in ADMIN_IPS

	return dict(year=datetime.now().year, show_admin_link=show_admin_link)

@frontend.route('/static/js/calc.js')
def calc_js():
	prices = Price.query.all()
	return render_template('frontend/calc.js', prices=prices, page_design_price=PAGE_DESIGN_PRICE)

@frontend.route('/')
def index():
	packs_prices = get_packs_prices()
	review = Review.query.order_by(db.func.random()).first()
	feeds = Work.query.filter_by(show_in_feed=True).order_by('-position')

	context = dict(
		main=True,
		packs_prices=packs_prices,
		review=review,
		feeds=feeds,
	)
	return render_template('frontend/index.html', **context)

@frontend.route('/portfolio/show/<slug>')
def portfolio_show(slug):
	type_ = request.args.get('type', None)

	work = Work.query.filter_by(slug=slug).first()
	if work.type_ == 'psd2html':
		markup = Markup.query.filter_by(work_id=work.id).first()
	else:
		markup = None

	if type_:
		prev_work = Work.query.filter_by(type_=type_)
		next_work = Work.query.filter_by(type_=type_).order_by('-position')
	else:
		prev_work = Work.query
		next_work = Work.query.order_by('-position')

	prev_work = prev_work.filter(Work.position > work.position).first()
	next_work = next_work.filter(Work.position < work.position).first()

	if work is None:
		work = Work.query.get(slug)

	type_ = request.args.get('type', None)

	back_to = '/portfolio'
	if type_:
		back_to = '{0}/{1}'.format(back_to, type_)

	context = dict(
		work=work,
		portfolio=True,
		back_to=back_to,
		prev_work=prev_work,
		next_work=next_work,
		type_=type_,
		markup=markup,
	)
	return render_template('frontend/portfolio_show.html', **context)

@frontend.route('/portfolio', defaults={ 'type_': 'all' })
@frontend.route('/portfolio/<type_>')
def portfolio(type_):
	xhr = 'ajax' in request.args

	page = request.args.get('page', 1)
	if page.__class__ in (str, unicode):
		# escaping from path's like '...?page=3/'
		# '/' raises error
		page = re.sub(r'[^\d]+', '', page)
		page = int(page)

	if type_ == 'all':
		pagination = Work.query.order_by('-position').paginate(page, WORKS_PER_PAGE)
	else:
		pagination = Work.query.order_by('-position').filter_by(type_=type_).paginate(page, WORKS_PER_PAGE)
	works = pagination.items

	all_works = Work.query.all()
	works_count = dict(
		all=len(all_works),
		design=len(filter(lambda x: x.type_ == 'design', all_works)),
		site=len(filter(lambda x: x.type_ == 'site', all_works)),
		psd2html=len(filter(lambda x: x.type_ == 'psd2html', all_works)),
	)

	if xhr:
		json_pagination = dict(page=pagination.page, has_prev=pagination.has_prev, has_next=pagination.has_next)
		return jsonify(
			portfolio_html=render_template('frontend/_port_list.html', works=works, type_=type_),
			pagination_html=render_template('frontend/_pagination.html', pagination=pagination, type_=type_),
			port_types_html=render_template('frontend/_port_types.html',
				type_=type_, works=works, works_count=works_count),
			port_shift_html=render_template('frontend/_port_shift.html', pagination=pagination),
			pagination=json_pagination,
		)
	else:
		context = dict(
			type_=type_,
			works=works,
			pagination=pagination,
			portfolio=True,
			works_count=works_count,
		)
		return render_template('frontend/portfolio.html', **context)


@frontend.route('/calculator', defaults={ 'pack': None })
@frontend.route('/calculator/<pack>')
def calc(pack):
	prices = Price.query.order_by('-by_default', '-position')
	packs_prices = get_packs_prices(prices)
	form = OrderForm()

	default_prices = filter(lambda p: p.by_default, prices)
	not_default_prices = filter(lambda p: not p.by_default, prices)

	context = dict(
		pack=pack,
		prices=prices,
		calc=True,
		packs_prices=packs_prices,
		default_prices=default_prices,
		not_default_prices=not_default_prices,
		form=form,
	)
	return render_template('frontend/calc.html', **context)

@frontend.route('/contacts')
def contacts():
	return u'Леша ленивый'

@frontend.route('/sitemap.xml')
def sitemap_xml():
	import math

	works = Work.query.all()
	total_pages = int(math.ceil(float(len(works)) / float(WORKS_PER_PAGE)))
	pages = (x + 1 for x in xrange(total_pages))
	return render_template('sitemap.xml', works=works, pages=pages)

@frontend.route('/send-feedback', methods=['POST'])
def send_feedback():
	prices = Price.query.order_by('-by_default', '-position')
	selected_prices = []
	data = request.form.copy()

	body = u'Выбранные модули:\n\n'
	total = 0

	for price in prices:
		if ('price%s' % price.id) in data or price.by_default:
			body = body + '- ' + price.title + '\n'
			total = total + price.price

	body = body + u'\nот %s руб.\n\n' % total
	body = body + u'Имя: %s\n\nКонтакты:\n%s\n\n' % (data['name'], data['contacts'])
	if data['comment'].strip():
		body = body + u'Комментарий:\n%s' % data['comment']

	if mail_app:
		message = Message(u'Обратная связь', sender='feedback@8hands.ru',
			recipients=['gkrasulya@gmail.com', 'no-thx@mail.ru', 'studio@8hands.ru'], body=body)
		mail_app.send(message)

	return body
	return 'ok'

@frontend.route('/get-review')
def get_review():
	review = Review.query.order_by(db.func.random()).first()

	return render_template('frontend/_review.html', review=review)

def get_packs_prices(prices=None):
	if prices is None:
		prices = Price.query.all()
	
	packs_prices = dict(
		card=[0, 0, []],
		catalog=[0, 0, []],
		shop=[0, 0, []],
	)
	packs = packs_prices.keys()
	total_pages = 0

	for price in prices:
		for pack in packs:
			if price.by_default or getattr(price, 'in_%s' % pack):
				# if price.code == 'main':
				# 	packs_prices[pack][0] += price.price
				# else:
				# 	packs_prices[pack][0] += price.price + price.min_pages * PAGE_DESIGN_PRICE
				# packs_prices[pack][1] += price.min_pages
				# if app.config['DEBUG']:
				# 	packs_prices[pack][2].append(dict(
				# 		price=price.price,
				# 		min_pages=price.min_pages,
				# 		id=price.id,
				# 	))
				packs_prices[pack][0] += price.price
	
	return packs_prices