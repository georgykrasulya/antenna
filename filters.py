from antenna import jinja_env

def datetime(value, format='%H:%M / %d-%m-%Y'):
    return value.strftime(format)

jinja_env.filters['datetime'] = datetime