# coding: utf-8

from wtforms import Form, SelectField, TextField, PasswordField, FileField, RadioField, BooleanField, validators, widgets

class LoginForm(Form):
	username = TextField(u'Логин', [validators.Required(u'Введите логин')])
	password = PasswordField(u'Пароль', [validators.Required(u'Введите пароль')])

class PageForm(Form):
	title = TextField(u'Заголовок страницы')
	text = TextField(u'Текст', widget=widgets.TextArea())

class CategoryForm(Form):
	title = TextField(u'Название', [validators.Required(u'Введите название')])

class ProductForm(Form):
	title = TextField(u'Название', [validators.Required(u'Введите название')])
	description = TextField(u'Описание', widget=widgets.TextArea(),
		description=u'Например, размеры и цвета модели. Не обязательное поле')
	photo = FileField(u'Фотография')
	price = TextField(u'Цена')
	category_id = SelectField(u'Категория', coerce=int, validators=[validators.Required(u'Выберите категорию')])
	in_stock = BooleanField(u'В наличии')

class PostForm(Form):
	CATEGORY_CHOICES = (
		('event', u'Событие'),
		('income', u'Поступление'),
	)
	title = TextField(u'Заголовок', [validators.Required(u'Введите заголовок')])
	text = TextField(u'Текст', [validators.Required(u'Введите текст')],
		widget=widgets.TextArea())
	category = RadioField(u'Категория', choices=CATEGORY_CHOICES)
	photo = FileField(u'Фотография')

class ProviderForm(Form):
	title = TextField(u'Заголовок', [validators.Required(u'Введите заголовок')])
	slug = TextField(u'Слаг', [validators.Required(u'Введите слаг')],
		description=u'Текст для ссылки. Например, tricolor.<br/> Тогда адрес будет http://antenna26.ru/providers/tricolor')
	photo = FileField(u'Фотография')
	description = TextField(u'Описание', [validators.Required(u'Введите описание')],
		widget=widgets.TextArea())
	channels = TextField(u'Каналы', [validators.Required(u'Введите каналы')],
		widget=widgets.TextArea())