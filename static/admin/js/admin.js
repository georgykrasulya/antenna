(function($) {

	var $deleteLinks = $('a.delete-row-link');

	$deleteLinks.click(function() {
		if (! confirm('Вы уверены?'))
			return false;
				
		var $self = $(this);
		$.get($self.attr('href'), { 'ajax': true }, function(res) {

			if (res == 'ok') {
				$.flash('Модель удалена');
				$self.parents('tr').eq(0).fadeOut();
			} else if (res == 'error') {
				$.flash('Произошла ошибка', 'error');
			}
		});
		
		return false;
	});

	var $moduleContainers = $('.module-container');

	$moduleContainers.each(function(i, el) {
		var $self = $(this),
			$h1 = $self.find('h1'),
			$closeOpen = $self.find('div.close-open');

		if ($.cookie('opened' + i) == 0) $closeOpen.hide();

		$h1.click(function() {
			if ($closeOpen.css('display') == 'none') {
				$closeOpen.show();
				$.cookie('opened' + i, 1);
			} else {
				$closeOpen.hide();
				$.cookie('opened' + i, 0);
			}
		});
	});

	var $hints = $('p.hint');

	$hints.each(function(i, el) {
		var $hint = $(el);

		if ($.cookie($hint.attr('id'))) {
			$hint.hide();
		} else {
			$hint.click(function() {
				var $self = $(this);

				$.cookie($self.attr('id'), true, {
					expires: 99,
					path: '/'
				});
				$self.hide();
			});
		}
	});

})(jQuery);