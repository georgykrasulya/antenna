# coding: utf-8

from flask import Module, session, flash, redirect, render_template, request, g
from functools import wraps
from flaskext import sqlalchemy

from antenna import app, db
from forms import *
from models import *

admin = Module(__name__)

def login_required(f):
	@wraps(f)
	def decorated(*args, **kwargs):
		if not logged():
			flash(u'Вы должны авторизоваться!', 'error')
			return redirect('/admin/login')
		return f(*args, **kwargs)
	return decorated

def logged():
	return session.get('logged', False)

@admin.route('/')
@login_required
def index():
	categories = Category.query.order_by('position').all()
	posts = Post.query.order_by('-id')
	providers = Provider.query.all()

	context = dict(
		categories=categories,
		posts=posts,
		providers=providers
	)
	return render_template('admin/index.html', **context)

@admin.route('/Product/new', methods=['GET', 'POST'])
@login_required
def add_product():
	form = ProductForm(request.form, category_id=request.args.get('category_id', None))
	categories = Category.query.all()
	form.category_id.choices = [(c.id, c.title) for c in categories]

	if request.method == 'POST' and form.validate():
		product = Product(
			title=request.form['title'],
			description=request.form['description'],
			photo=request.files['photo'],
			price=request.form['price'],
			category_id=request.form['category_id'],
			in_stock='in_stock' in request.form,
		)
		db.session.add(product)
		db.session.commit()
		flash(u'Модель "%s" добавлена' % product.title)
		redirect_url = '/admin/Product/new?category_id=%s' % request.form['category_id'] \
			if '_add_more' in request.form \
			else '/admin/Category/%s' % request.form['category_id']
		return redirect(redirect_url)

	context = dict(
		form=form,
		title=u'Добавление модели',
		add=True,
		cls_name='Product',
	)

	return render_template('admin/manage_record.html', **context)

@admin.route('/Product/<int:id>', methods=['GET', 'POST'])
@login_required
def edit_product(id):
	product = Product.query.get(id)
	categories = Category.query.all()
	form = ProductForm(request.form, obj=product)
	form.category_id.choices = [(c.id, c.title) for c in categories]

	if request.method == 'POST' and form.validate():

		product.title = request.form['title']
		product.description = request.form['description']
		product.category_id = request.form['category_id']
		product.in_stock = 'in_stock' in request.form
		product.price = request.form['price']

		if 'photo' in request.files and request.files['photo']:
			product.save_photo(request.files['photo'])

		db.session.add(product)
		db.session.commit()
		flash(u'Модель "%s" изменена' % product.title)
		redirect_url = '/admin/Category/%s' % product.category_id
		return redirect(redirect_url)

	context = dict(
		form=form,
		title=u'Редактирование модели "%s"' % product.title,
		record=product,
		cls_name='Product',
	)

	return render_template('admin/manage_record.html', **context)

@admin.route('/<cls_name>/new', methods=['GET', 'POST'])
@login_required
def add_record(cls_name):

	FormClass = globals()['%sForm' % cls_name]
	ModelClass = globals()[cls_name]

	form = FormClass(request.form)

	if request.method == 'POST' and form.validate():
		data = {}
		for kw, val in request.form.iteritems():
			data[kw] = val
		if 'photo' in request.files:
			data['photo'] = request.files['photo']
		record = ModelClass(**data)

		db.session.add(record)
		db.session.commit()
		flash(u'Запись добавлена')

		if '_add_more' in request.form:
			redirect_url = '/admin/%s/new' % cls_name
		else:
			redirect_url = '/admin'
		return redirect(redirect_url)

	context = dict(
		form=form,
		add=True,
		cls_name=cls_name
	)

	return render_template('/admin/manage_record.html', **context)

@admin.route('/<cls_name>/<int:id>', methods=['GET', 'POST'])
@login_required
def edit_record(cls_name, id):
	FormClass = globals()['%sForm' % cls_name]
	ModelClass = globals()[cls_name]

	record = ModelClass.query.get(id)
	form = FormClass(request.form, obj=record)

	if request.method == 'POST' and form.validate():
		record.setattrs(**request.form)

		if 'photo' in request.files and request.files['photo']:
			record.save_photo(request.files['photo'])

		db.session.add(record)
		db.session.commit()
		flash(u'Запись изменена')

		if '_add_more' in request.form:
			redirect_url = '/admin/%s/new' % cls_name
		else:
			redirect_url = '/admin'
		return redirect(redirect_url)

	context = dict(
		record=record,
		form=form,
		cls_name=cls_name,
	)

	return render_template('/admin/manage_record.html', **context)

@admin.route('/<cls_name>/delete/<int:id>')
@login_required
def delete_record(cls_name, id):
	ModelClass = globals()[cls_name]

	is_ajax = request.is_xhr

	if not id is None:
		try:
			record = ModelClass.query.get(id)
		except sqlalchemy.orm.exc.UnmappedInstanceError:
			record = None
			
		if record:
			db.session.delete(record)
			db.session.commit()

			if is_ajax:
				return 'ok'
			else:
				flash(u'Модель была удалена')
				return redirect('/admin')
	if is_ajax:
		return 'error'
	else:
		flash(u'Произошла ошибка', 'error')
		return redirect('/admin')

@admin.route('/category/change-position', methods=['POST'])
@login_required
def change_category_position():
	ids = request.form['ids'].split(',')

	s = ''
	for position, id in enumerate(ids):
		s = s + '%s: %s' % (id, position)
		db.engine.execute('UPDATE categories SET position = ? WHERE id = ?', position, id)

	return s

@admin.route('/product/change-position', methods=['POST'])
@login_required
def change_product_position():
	ids = request.form['ids'].split(',')

	s = ''
	for position, id in enumerate(ids):
		s = s + '%s: %s' % (id, position)
		db.engine.execute('UPDATE products SET position = ? WHERE id = ?', position, id)

	return s

@admin.route('/login', methods=['GET', 'POST'])
def login():
	form = LoginForm(request.form)

	if request.method == 'POST' and form.validate():
		if (request.form['username'] == app.config['ADMIN_USERNAME'] and
		request.form['password'] == app.config['ADMIN_PASSWORD']):
			flash(u'Вы усешно авторизовались.')
			session['logged'] = True
			return redirect('/admin')
		flash(u'Неправильные логин и/или пароль.', 'error')

	body_class = 'class="login"'

	context = dict(
		form=form,
		body_class=body_class,
		not_logged=True,
	)

	return render_template('admin/login.html', **context)

@admin.route('/admin/logout')
def logout():
	session['logged'] = False
	return render_template('admin/logout.html', not_logged=True)