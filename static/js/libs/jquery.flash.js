(function($) {
	$.flash = function(message, opts) {
		var opts = $.extend({ 
    		duration: 	500,
    		timeout:	10000,
    		opacity: 	.9,
    		type:		'',
    		side:		'bottom', // top, bottom
    		type:		'out',
    		easing: 	'swing',
    		altClass:		'',
    		width: 300
      }, opts || {});
      
    if (opts.altClass != '') opts.altClass += ' ';
    
    var w = $(window),
    	d = $(document),
   		flashTimeout = w.data('flashTimeout'),
   		flashMessage = w.data('flashMessage');
    
    if (flashTimeout) {
    	clearTimeout(flashTimeout);
    }

    if (!flashMessage) {
    	var flashMessageHtml = [
    		'<div class="flash-message">',
    		'<div class="flash-message-body ' + opts.altClass + ' ' + opts.type + '">',
    		'<div class="flash-message-close">x</div>',
    		'<div class="flash-message-text"></div>',
    		'</div></div>'
    	];
    	flashMessageHtml = flashMessageHtml.join("");
    	
			var flashMessage = $(flashMessageHtml);
    		
			flashMessage.css({
				position: 'absolute'
			});
			w.data('flashMessage', flashMessage);
	        
			$('body').append(flashMessage);
			
    	var flashMessageBody =	flashMessage.find(".flash-message-body"),
    		flashMessageText =	flashMessage.find(".flash-message-text");
    	
    	d.scroll(function() {
    		reposition();
    	});
    } else {
    	
    	flashMessage.attr('class', 'flash-message ' + opts.altClass + opts.type);
    	var flashMessageBody =	flashMessage.find(".flash-message-body"),
    		flashMessageText =	flashMessage.find(".flash-message-text");
    }
		
    flashMessageText.html(message);
    flashMessageBody.click(function() {
  		$(this).fadeOut();
  	});
    	
		flashMessageBody.css("display", "block");
		
		var top = (w.height() / 8 * 7) - (flashMessage.height() / 2),
			left = (w.width() - flashMessage.width()) / 2 + w.scrollLeft();
    
    reposition();
		
		flashMessageBody.css({
			top: -100,
			opacity: 0
		}).animate({
			top: 0,
			opacity: opts.opacity
		}, opts.duration, opts.easing);
		
		w.data('flashTimeout', setTimeout(function() {
			flashMessageBody.fadeOut('normal');
		}, opts.timeout));
		
		function reposition() {
			flashMessage.css({
				top: top + d.scrollTop(),
				left: left
			});
		}
		
	};
	
})(jQuery);