/* Author: Georgy Krasulya

*/

window.addEvent('domready', function() {

	var b = $$('body')[0],
		H = window.History;

	var feedBar = $('feedBar');

	if (feedBar) {

		var feedScroll = $('feedScroll'),
		feedScrollInner = feedScroll.getFirst(),
		feedsLength = feedScrollInner.getChildren().length,
		feedControls = $('feedControls').getElements('a');

		feedBar.setStyle('display', 'block');

		feedScrollInner.setStyles({
			'width': feedsLength * 255,
			'left': 0
		});

		var feedScrollHandler = new ScrollBar(feedScroll, 'feedBar', 'feedKnob', {
		    scroll: {
		        duration: 255,
		        transition: 'quad:out',
		        onStart: function() {},
		        onComplete: function() {}
		    },
		    slider: {
				wheel: false,
		        offset: -1,
		        onChange: function(pos) {},
		        onComplete: function(pos) {}
		    },
		    knob: {
		        duration: 255,
		        transition: 'quad:out',
		        onStart: function() {}
		    }
		});

		feedControls.addEvent('click', function() {
			var shift = this.hasClass('feed-mask-left') ? -100 : 100;

			feedScrollHandler.move2(shift);
			
			return false;	
		});

		window.f = feedScrollHandler;

	} // if feedBar

	var topLogo = $('topLogo'),
		topLogoFx = new Fx.Morph(topLogo, {
			'link': 'cancel',
			'duration': 250,
			'transition': 'quad:out'
		}),
		slogan = topLogo.getFirst(),
		slogans = [
			'Мы делаем <strong>хорошие</strong> сайты',
			'Мы делаем сайты <strong>иначе</strong>',
			'4-х кратная <strong>эффективность</strong>'
		],
		first = true;

	slogan.set({
		// html: slogans[Math.round(Math.random() * 2)],
		styles: {
			opacity: 0,
			display: 'inline'
		},
		tween: {
			duration: 1000
		}
	});
	slogan.tween('opacity', 1);


	topLogo.addEvents({
		mouseenter: function() {
			if (first) {
				topLogoFx.start({
					top: [0, -15],
					opacity: [1, 0.5]
				});
			} else {
				topLogoFx.start({
					top: -15,
					opacity: 0.5
				});
			}
		},
		mouseleave: function() {
				topLogoFx.start({
					top: 0,
					opacity: 1
				});
		}
	});


	// Spaceman. Move top
	// yo

	var windowHeight = window.getSize().y,
		spaceman = $('spaceman'),
		visible = false,
		windowFx = new Fx.Scroll(window, {
			transition: 'quad:out'
		});

	spaceman.set({
		'morph': {
			duration: 200,
			transition: 'quad:out',
			link: 'chain'
		},
		'styles': {
			'opacity': 0.5
		}
	});

	spaceman.addEvents({
		mouseenter: function() {
			this.morph({
				opacity: 1
			});
		},
		mouseleave: function() {
			this.morph({
				opacity: 0.5
			});
		},
		click: function() {
			windowFx.toTop();
			
			return false;
		}
	});

	window.addEvent('scroll', function() {
		var scrollTop = window.getScroll().y;

		if (! visible && scrollTop > windowHeight) showSpaceman();
		if (visible && scrollTop < windowHeight) hideSpaceman();
	});

	function showSpaceman() {
		spaceman.morph({
			bottom: 10
		});
		visible = true;
	}

	function hideSpaceman() {
		spaceman.morph({
			bottom: -129
		});
		visible = false;
	}

	// calc packages
	// yo
	var packs = $('packs'),
		currentPackIndex;

	if (packs) {
		var packLinks = packs.getChildren('a'),
			isMain = b.hasClass('main'),
			maxHeight = 0;

		packLinks.each(function(pack) {
			var height = pack.getSize().y;
			if (maxHeight < height) maxHeight = height;
		});
		packLinks.setStyle('height',
			maxHeight - parseInt(packLinks[0].getStyle('padding-top')) - parseInt(packLinks[0].getStyle('padding-bottom')));
	}

	// reviews on main page
	// yo

	var review = $('review');

	if (review) {
		var reviewMore = $('reviewMore'),
			reviewMoreText = reviewMore.get('html'),
			reviewOldMarginTop = review.getStyle('margin-top'),
			currentReview = 1,
			reviewRequest = new Request.HTML({
				url: '/get-review',
				method: 'get',

				onSuccess: function(t, e, html) {
					review.setStyles({
						opacity: 0,
						marginTop: parseInt(reviewOldMarginTop, 10) + 70
					});

					review.set('html', html);

					review.morph({
						opacity: 1,
						marginTop: reviewOldMarginTop
					});

					reviewMore.set('html', reviewMoreText);
					reviewMore.removeClass('loading');

					currentReview++;
				}
			});

		review.set('morph', {
			duration: 250,
			transition: 'quad:out'
		});

		reviewMore.addEvent('click', function(event) {
			reviewMore.set('html', 'Секундочку...');
			reviewMore.addClass('loading');
			reviewRequest.send({
				data: currentReview
			});

			return false;
		});

	} // if (review) i.e. main page


	// portfolio
	// inserting div.c for each row
	var portList = $('portList');

	if (portList) {

		var portTypesArr = ['all', 'design', 'site', 'psd2html'],
		portTypes = $('portTypes').getElements('a');

		portTypes.each(function(type, i) {
			if (type.hasClass('selected')) currentType = portTypesArr[i];
		});

		function insertDivC() {
			portList.getElements('div.port-item').each(function(item, i) {
				if (i % inRow == 0) {
					var divC = new Element('<div></div>', {
						'class': 'c'
					});
					divC.inject(item, 'before');
				}
			});
		}

		function portLinksClick(isType, href) {
			return function() {
				if (isType) {
					portTypes.removeClass('selected');
					paginationLinks.removeClass('selected');
					paginationLinks[0] && paginationLinks[0].addClass('selected');

					var hrefArr = this.href.split('/'),
						type = hrefArr[hrefArr.length-1];

					currentType = type;

					portTypes.each(function(el, i) {
						if (portTypesArr[i] == currentType) {
							el.addClass('selected');
						}
					});
				} else {
					paginationLinks.removeClass('selected');
					// currentPage = this.href.match(/page=(\d+)/)[1];
				}
				if (! History.emulated.pushState)
					History.pushState({}, '', this.href); // changing uri

				this.addClass('selected');

				portList.morph({
					opacity: 0.2
				});
				
				var ajaxArg = this.href.indexOf('\?') != -1 ? '&ajax' : '?ajax';
				portRequest.setOptions({
					url: this.href + ajaxArg
				}).send();

				return false;
			};
		}

		function initPortfolioPage() {
			if (History.emulated.pushState) {
				var hash = History.getHash();
				hash = hash.replace(/^[\/\.\#]+/, '');

				//	portfolio
				//	portfolio/psd2html
				//	portfolio?page=2
				//	portfolio/psd2html?page=2
				if (~hash.search(/portfolio(\/psd2html|design|all|site)?(\?page=\d+)?/)) {
					portLinksClick(!(~hash.search(/page=\d+/)), hash)();
				}

			}
		}

		initPortfolioPage();

		var portItems = portList.getElements('div.port-item'),
			portItemTypes = portList.getElements('div.type a'),
			pagination = $('pagination'),
			paginationLinks = pagination.getElements('a'),
			portShiftLinks = $$('#portShiftHandler a'),
			listFx = new Fx.Morph(portList, {
				duration: 50,
				link: 'cancel'
			}),
			portRequest = new Request.JSON({
				method: 'get',
				link: 'cancel',
				onSuccess: function(data, text) {

					pagination.set('html', data.pagination_html);
					pagination.getElements('a').addEvent('click', portLinksClick(false));

					portList.set('html', data.portfolio_html);
					portList.getElements('div.type a').addEvent('click', portLinksClick(true));

					portList.morph({
						opacity: 1
					});

					insertDivC();

					windowFx.toElement(portList);
				}
			});


		insertDivC(); // insert <div class="c"></div> after each line

		portTypes.addEvent('click', portLinksClick(true)); // type menu under the title
		portItemTypes.addEvent('click', portLinksClick(true)); // types in each portfolio item
		paginationLinks.addEvent('click', portLinksClick(false)); // page links

		portShiftLinks.addEvent('click', function() {
			var next = this.hasClass('next');
			next ? currentPage++ : currentPage--;

			var url = '/get-port-list/{0}/{1}'
					.substitute([currentType, currentPage]);

			paginationLinks.removeClass('selected');
			paginationLinks[currentPage-1].addClass('selected');

			portList.morph({
				opacity: 0.2
			});
			
			portRequest.setOptions({
				url: url
			}).send();

			return false;
		});

	}

});











