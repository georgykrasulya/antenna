# coding: utf-8

import json

from flask import Module, session, flash, redirect, render_template, request, g, abort
from flaskext import sqlalchemy

from antenna import app, db
from models import *

frontend = Module(__name__)

POSTS_PER_PAGE = 5

# setting template variables
@frontend.context_processor
def get_providers():
	providers = Provider.query.all()
	first_category = Category.query.first()

	return dict(
		providers=providers,
		first_category=first_category
	)

@frontend.route('/', defaults=dict(filter_by=None))
@frontend.route('/<filter_by>')
def index(filter_by=None):
	posts = Post.query.order_by('-id')
	if filter_by is not None:
		posts = posts.filter_by(category=filter_by)
	total = posts.count()
	posts = posts[:POSTS_PER_PAGE]
	show_else = total > len(posts)

	return render_template('frontend/index.html', posts=posts, main_page=True,
		filter_by=filter_by, show_else=show_else, index=True)

@frontend.route('/sputnikovoetv/<slug>')
def provider(slug):
	provider = Provider.query.filter_by(slug=slug).scalar()
	if not provider:
		abort(404)

	return render_template('frontend/provider.html', provider=provider)

@frontend.route('/faq')
def faq():
	return render_template('frontend/faq.html', faq_page=True)

@frontend.route('/about')
def about():
	return render_template('frontend/about.html', about_page=True)

@frontend.route('/contacts')
def contacts():
	return render_template('frontend/contacts.html', contacts_page=True)

@frontend.route('/catalog/<id>')
def catalog(id):
	main_category = Category.query.get(id)
	categories = Category.query.order_by('position').all()
	products = main_category.products.order_by('position')

	return render_template('frontend/catalog.html',
		main_category=main_category, products=products, categories=categories,
		catalog_page=True)

@frontend.route('/sitemap')
def sitemap():
	return render_template('frontend/sitemap.html')

@frontend.route('/posts-list/<int:page>', defaults=dict(filter_by=None))
@frontend.route('/posts-list/<int:page>/<filter_by>')
def posts_list(page, filter_by):
	posts = Post.query.order_by('-id')
	if filter_by is not None:
		posts = posts.filter_by(category=filter_by)
	total = posts.count()
	posts = posts[(page-1)*POSTS_PER_PAGE:page*POSTS_PER_PAGE]

	html = []
	for post in posts:
		html.append(render_template('frontend/_post.html', post=post))
	html = "\n".join(html)
	last = page * POSTS_PER_PAGE >= total

	return json.dumps(dict(html=html, last=last))

'''
@frontend.route('/product-list')
def product_list():
	if request.is_xhr:
		slug = request.args.get('slug', None)
		if slug is None:
			return 'error'
		category = Category.query.filter_by(slug=slug)[0]
		html = []
		for product in category.products.all():
			html.append(render_template('frontend/_product.html', product=product))
		return "\n".join(html)
	else:
		return ''
'''

@frontend.route('/robots.txt')
def robots():
	return render_template('frontend/robots.txt')

@frontend.route('/sitemap.xml')
def sitemap_xml():
	providers = Provider.query.all()
	categories = Category.query.all()
	return render_template('frontend/sitemap.xml', providers=providers,
		categories=categories)
