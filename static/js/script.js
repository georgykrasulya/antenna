/* Author: 

*/



(function() {

	var $print = $('#print');

	$print.click(function() {
		window.print();

		return false;
	});

	var $newsList = $('#newsList');

	function bindNewsListMore(open) {
		open = typeof open != 'undefined';

		$newsList.find('a.more').each(function(i, el) {
			var $newsItem = $(el).parents('div.news-item:eq(0)'),
				$span = $(el).find('span');

			$(el).click(function() {
				if ($newsItem.hasClass('opened')) {
					$newsItem.removeClass('opened');
					$span.html('Раскрыть');
				} else {
					$newsItem.addClass('opened');
					$span.html('Свернуть');
				}

				return false;
			});

			if (open) $(el).click();
		});

	};
	bindNewsListMore(true);

	var $logo = $('#logo');

	$logo.hover(function() {
		$logo.animate({
			top: -10,
			opacity: 0.5
		}, 200);
	}, function() {
		$logo.stop().animate({
			top: 0,
			opacity: 1
		});
	});

	var $newsMore = $('#newsMore'),
		currentPage = 1,
		loading = false;

	$newsMore.click(function() {
		if (loading) return false;
		loading = true;
		var $self = $(this);

		$self.html('загружается...');

		currentPage++;

		var url = '/posts-list/' + currentPage;
		if (filter_by != 'None')
			url += '/' + filter_by;
		$.get(url, function(res) {
			$newsList.html($newsList.html() + res.html);
			$self.html('Ёще новости');

			if (res.last) $self.remove();
			loading = false;

			bindNewsListMore();
		}, 'json');

		return false;
	});

	var $faqList = $('#faqList'),
		$faqItems = $faqList.find('div.faq-item'),
		$current = $faqItems.eq(0).find('div.faq-a');

	$faqItems.each(function(i, el) {
		var $q = $(this).find('.faq-q a'),
			$a = $(this).find('.faq-a');

		i != 0 && $a.hide();

		$q.click(function() {
			if ($a.css('display') == 'none') {
				$a.slideDown();

				if ($current && $current !== $a) $current.slideUp();
				$current = $a;
			} else {
				$a.slideUp();
			}

			return false;
		});
	});

	var $w = $(window),
		$innerBody = $('.inner-body'),
		$footer = $('footer');


	$w.load(function() {
		if ($innerBody.height() < $w.height()) {
			$innerBody.height($w.height());

			$footer.css({
				position: 'absolute',
				bottom: 0
			});
		}
	});

})();











