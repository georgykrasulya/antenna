import re
import os
import time
from datetime import datetime
from PIL import Image

from antenna import db, products_set, posts_set, providers_set

'''
class Page(db.Model):
	__tablename__ = 'pages'

	id = db.Column(db.Integer, primary_key=True)
	title = db.Column(db.String(255))
	text = db.Column(db.Text)
	text_html = db.Column(db.Text)
	slug = db.Column(db.String(50))

	def description(self):
		return '%s...' % self.text[:100]

	def __init__(self, title, text):
		self.title = title
		self.text = text

	def __repr__(self):
		return '<Page: %s>' % self.title
'''

class SetAttrsModel:
	def setattrs(self, **kwargs):
		for kw, val in kwargs.iteritems():
			if hasattr(self, kw): setattr(self, kw, val[0])


class Product(db.Model, SetAttrsModel):
	__tablename__ = 'products'

	id = db.Column(db.Integer, primary_key=True)
	title = db.Column(db.String(255))
	filename = db.Column(db.String(100))
	description = db.Column(db.Text)
	date_added = db.Column(db.DateTime)
	in_stock = db.Column(db.Boolean)
	position = db.Column(db.Integer)
	price = db.Column(db.Integer)

	category_id = db.Column(db.Integer, db.ForeignKey('categories.id'))
	category = db.relationship('Category',
		backref=db.backref('products', lazy='dynamic'))

	def __init__(self, **kwargs):
		if kwargs:
			for kw, val in kwargs.iteritems():
				if hasattr(self, kw): setattr(self, kw, val)
			if 'photo' in kwargs and kwargs['photo']:
				self.save_photo(kwargs['photo'])
			if not 'date_added' in kwargs:
				self.date_added = datetime.utcnow()

	def __repr__(self):
		return '<Product %r>' % self.title

	def save_photo(self, photo):
		self.filename = products_set.save(photo)
		create_thumbnail(self.path(), self.path(), 1000)

	def url(self, size=None):
		if not self.filename:
			return
		
		url = products_set.url(self.filename)
		if not size is None:
			url = url.rsplit('.', 1)
			url = '{0}_{1}.{2}'.format(url[0], size, url[1])
			path = self.path(size)
			if not os.path.isfile(path):
				create_thumbnail(self.path(), path, size)
		return url

	def path(self, size=None):
		path = products_set.path(self.filename)
		if not size is None:
			path = path.rsplit('.', 1)
			path = '{0}_{1}.{2}'.format(path[0], size, path[1])
			if not os.path.isfile(path):
				create_thumbnail(self.path(), path, size)
		return path


class Category(db.Model, SetAttrsModel):
	__tablename__ = 'categories'

	id = db.Column(db.Integer, primary_key=True)
	title = db.Column(db.String(255))
	position = db.Column(db.Integer, default=int(time.time()))

	def __init__(self, **kwargs):
		if kwargs:
			for kw, val in kwargs.iteritems():
				if hasattr(self, kw):
					setattr(self, kw, val)

	def __repr__(self):
		return '<Category: %s>' % self.title

class Post(db.Model, SetAttrsModel):
	__tablename__ = 'posts'

	id = db.Column(db.Integer, primary_key=True)
	title = db.Column(db.String(255))
	filename = db.Column(db.String(100))
	text = db.Column(db.Text)
	date_added = db.Column(db.DateTime)
	category = db.Column(db.String(50))

	def __init__(self, **kwargs):
		if kwargs:
			for kw, val in kwargs.iteritems():
				if hasattr(self, kw): setattr(self, kw, val)
			if 'photo' in kwargs and kwargs['photo']:
				self.save_photo(kwargs['photo'])
			if not 'date_added' in kwargs:
				self.date_added = datetime.utcnow()

	def __repr__(self):
		return '<Product %r>' % self.title

	def text_html(self):
		return text2html(self.text)

	def description(self):
		return self.text[:100]

	def short_date(self):
		return str(self.date_added)[:10].replace('-', '.')

	def save_photo(self, photo):
		self.filename = posts_set.save(photo)
		create_thumbnail(self.path(), self.path(), 1000)

	def url(self, size=None):
		if not self.filename:
			return
		
		url = posts_set.url(self.filename)
		if not size is None:
			url = url.rsplit('.', 1)
			url = '{0}_{1}.{2}'.format(url[0], size, url[1])
			path = self.path(size)
			if not os.path.isfile(path):
				create_thumbnail(self.path(), path, size)
		return url

	def path(self, size=None):
		path = posts_set.path(self.filename)
		if not size is None:
			path = path.rsplit('.', 1)
			path = '{0}_{1}.{2}'.format(path[0], size, path[1])
			if not os.path.isfile(path):
				create_thumbnail(self.path(), path, size)
		return path

class Provider(db.Model, SetAttrsModel):
	__tablename__ = 'providers'

	id = db.Column(db.Integer, primary_key=True)
	title = db.Column(db.String(255))
	slug = db.Column(db.String(100))
	filename = db.Column(db.String(100))
	description = db.Column(db.Text)
	channels = db.Column(db.Text)
	date_added = db.Column(db.DateTime)

	def __init__(self, **kwargs):
		if kwargs:
			for kw, val in kwargs.iteritems():
				if hasattr(self, kw): setattr(self, kw, val)
			if 'photo' in kwargs and kwargs['photo']:
				self.save_photo(kwargs['photo'])
			if not 'date_added' in kwargs:
				self.date_added = datetime.utcnow()

	def __repr__(self):
		return '<Product %r>' % self.title

	def description_html(self):
		return text2html(self.description)

	def channels_html(self):
		#s = ''
		#s = self.channels.split("\n")
		#s = '</span>\n<span class="l">- '.join(s)
		#return '<span class="l">- %s</span>' % s
		return self.channels.replace("\n", '<br/>')

	def save_photo(self, photo):
		self.filename = providers_set.save(photo)
		create_thumbnail(self.path(), self.path(), 1000)

	def url(self, size=None):
		if not self.filename:
			return
		
		url = providers_set.url(self.filename)
		if not size is None:
			url = url.rsplit('.', 1)
			url = '{0}_{1}.{2}'.format(url[0], size, url[1])
			path = self.path(size)
			if not os.path.isfile(path):
				create_thumbnail(self.path(), path, size)
		return url

	def path(self, size=None):
		path = providers_set.path(self.filename)
		if not size is None:
			path = path.rsplit('.', 1)
			path = '{0}_{1}.{2}'.format(path[0], size, path[1])
			if not os.path.isfile(path):
				create_thumbnail(self.path(), path, size)
		return path


def text2html(text):
	return re.sub('\n', '<br/>\n', text)

def create_thumbnail(orig_path, path, size):
	img = Image.open(orig_path)
	img.thumbnail((size, size), Image.ANTIALIAS)
	img.save(path)
